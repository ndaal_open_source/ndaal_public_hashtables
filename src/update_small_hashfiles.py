#!/usr/bin/env python3
# encoding: utf-8
"""
Download hashsets and extract the relevant infomation.

The hashtables downloaded are available in different formats. The
relevant information is extracted and stored in the hashlookup-format
version 3 specified by the IETF, c.f.

https://datatracker.ietf.org/doc/html/draft-dulaunoy-hashlookup-format-03

Author: André Breuer
Created: 2023-05-17
Updated: 2023-05-17
Tested On: MacOS Ventura ARM architecture, Debian 11.X x86 architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"


import json
import os
import re
import zipfile
from shutil import rmtree
from typing import Union

import pandas as pd
import requests


def _check_response(response: requests.models.Response) -> None:
    """Check the response code of a request that has been made.

    :param response: Response of a request made.
    :type response: requests.models.Response

    :raises requests.exceptions.HTTPError: In case the status code
    indicates that the result retrieved was not ok.
    """
    if response.status_code != requests.codes.OK:  # pylint: disable=no-member
        raise requests.exceptions.HTTPError(
            f"Response is not okay. Got response code {response.status_code}"
        )


def _save_json(
    content: Union[list, dict],
    file_path: str,
    encoding: str = "utf-8",
    indent: int = 2,
) -> None:
    """Save content as JSON file.

    :param content: Contains the JSON content. If it is a list, then
        the list has to contain dicts as elements.
    :type content: Union[list, dict]
    :param file_path: Path to the JSON file where content should be
        saved.
    :type file_path: str
    :param encoding: Encoding to be used for storing the content in the
        JSON file, defaults to "utf-8".
    :type encoding: str, optional
    :param indent: Indent to be used in the formatted JSON, defaults
        to 2.
    :type indent: int, optional
    """
    with open(file_path, "w", encoding=encoding) as f:
        json.dump(content, f, indent=indent)


def _extract_zip(file_path: str, extract_dir: str) -> None:
    """Extract a given ZIP file.

    :param file_path: Path to the ZIP file.
    :type file_path: str
    :param extract_dir: Target directory to which the content should
        be extracted.
    :type extract_dir: str
    """
    if not os.path.exists(extract_dir):
        print(f"The directory '{extract_dir}' does not exist. Creating it")
        os.makedirs(extract_dir, exist_ok=True)

    with zipfile.ZipFile(file_path, "r") as z:
        z.extractall(extract_dir)


def _write_bytes_to_file(content: bytes, file_path: str) -> None:
    """Save a bytes object in a file.

    :param content: Content that should be stored.
    :type content: bytes
    :param file_path: Path under which the content should be stored.
    :type file_path: str
    """
    with open(file_path, "wb") as f:
        f.write(content)


def _read_file_lines(file_path: str, encoding: str = "utf-8") -> list:
    """Read content of a file line by line and return lines as a list.

    :param file_path: Source file to be used for reading.
    :type file_path: str
    :param encoding: Encoding to be used for reading the file, defaults
        to "utf-8".
    :type encoding: str, optional

    :return: List of strings containing the contents of the lines.
    :rtype: list
    """
    with open(file_path, encoding=encoding) as f:
        data = f.read()

    return data.split("\n")


def _delete_single_path(path: str) -> None:
    """Delete a file or directory under the given path.

    :param path: Path whose content should be deleted.
    :type path: str
    """
    if os.path.isfile(path):
        os.remove(path)
    else:
        rmtree(path)


def _delete_paths(paths: Union[str, list]) -> None:
    """Delete a single or multiple files or directories.

    :param paths: A string is interpreted as a single file or directory
        to be delted, whereas if a list is given it is looped over all
        the strings within the list and they are tried to be removed.
    :type paths: Union[str, list]

    :raises TypeError: If no string or list is given.
    """
    if isinstance(paths, str):
        _delete_single_path(paths)
    elif isinstance(paths, list):
        for path in paths:
            _delete_single_path(path)
    else:
        raise TypeError(
            "Given input is of incorrect type. Expected either str or list."
            f" Found {type(paths)}."
        )


def _get_directory_content(directory: str) -> list:
    """Get all files and directories within a given directory.

    :param directory: Directory to query for contents.
    :type directory: str

    :return: Strings containing all the files and directories within
        the given directory.
    :rtype: list
    """
    return [os.path.join(directory, data) for data in os.listdir(directory)]


def _extract_kaspersky_hashset(kaspersky_dir: str) -> dict:
    """Extract the Kaspersky hashsets from a given directory.

    :param kaspersky_dir: Directory where raw Kaspersky dataset is
        stored.
    :type kaspersky_dir: str
    :return: JSON content of the hashset.
    :rtype: dict
    """
    data = _get_directory_content(kaspersky_dir)
    for item in data:
        if "RDS" in item:
            files = os.listdir(item)
            files = [
                os.path.join(item, f)
                for f in files
                if os.path.isfile(os.path.join(item, f))
            ]
            for f in files:
                if "file" in f.casefold():
                    df = pd.read_csv(f)
                    hashtable = df.to_dict(orient="records")
        _delete_paths(item)

    return hashtable


def get_kaspersky_hashset() -> None:
    """Download and extract the Kaspersky hashset to JSON format."""
    KASPERSKY_URL = "https://s3.amazonaws.com/rds.nsrl.nist.gov/kaspersky/current/Diskprint_Kaspersky.zip"  # pylint: disable=line-too-long
    BASE_DIR = "dataset/kaspersky/"
    kaspersky_zip = os.path.join(BASE_DIR, "Diskprint_Kaspersky.zip")

    response = requests.get(KASPERSKY_URL, timeout=30)
    _check_response(response)

    os.makedirs(BASE_DIR, exist_ok=True)
    _write_bytes_to_file(response.content, kaspersky_zip)
    _extract_zip(kaspersky_zip, BASE_DIR)
    hashtable = _extract_kaspersky_hashset(BASE_DIR)
    _save_json(hashtable, os.path.join(BASE_DIR, "hashtable.json"))


def _get_paths(item: str) -> dict:
    """Get paths for sample hashsets.

    :param item: String containing MD5 path.
    :type item: str

    :return: Paths for MD5, SHA1, and SHA256 raw hashsets.
    :rtype: dict
    """
    return {
        "md5": item,
        "sha1": item.replace("MD5", "SHA1"),
        "sha256": item.replace("MD5", "SHA256"),
    }


def _create_hashtable(paths: dict) -> dict:
    """Create hashtable from raw sample hashsets.

    :param paths: Paths to the individual hashsets.
    :type paths: dict

    :return: JSON content of hashsets.
    :rtype: dict
    """
    md5_lines = _read_file_lines(paths.get("md5"))[:-1]
    sha1_lines = _read_file_lines(paths.get("sha1"))[:-1]
    sha256_lines = _read_file_lines(paths.get("sha256"))[:-1]
    hashtable = pd.DataFrame(
        list(zip(md5_lines, sha1_lines, sha256_lines)),
        columns=["MD5", "SHA-1", "SHA-256"],
    )
    hashtable.MD5 = hashtable.MD5.str.upper()
    hashtable["SHA-1"] = hashtable["SHA-1"].str.upper()
    hashtable["SHA-256"] = hashtable["SHA-256"].str.upper()

    return hashtable.to_dict(orient="records")


def _convert_hashset_sample_files(hashset_dir: str) -> None:
    """Convert all of the hashset sample files and store them.

    :param hashset_dir: Directory where the raw hash dataset is stored.
    :type hashset_dir: str
    """
    data = _get_directory_content(hashset_dir)
    for item in data:
        if ".json" in item:
            continue
        if "Raw" in item:
            if "MD5" in item:
                paths = _get_paths(item)
                hashtable = _create_hashtable(paths)
                file_path = item.replace("Raw_MD5_", "").replace(
                    ".txt", ".json"
                )
                _save_json(hashtable, file_path)
                _delete_paths(list(paths.values()))
            continue

        _delete_paths(item)


def get_hashset_sample() -> None:
    """Download and extract the Hashset samples into JSON format."""
    SAMPLE_HASH_URL = "https://www.hashsets.com/download/12/selected-tryout-files/4165/sample-pre-built-hash-sets-1000-rows-only-each.zip"  # pylint: disable=line-too-long
    BASE_DIR = "dataset/sample_hash_sets"
    sample_hash_zip = os.path.join(BASE_DIR, "sample_hashes.zip")

    # Disable verification of chain of trust for now
    # https://www.ssllabs.com/ssltest/analyze.html?d=www.hashsets.com
    #
    # Thus the verification is said to be not an issue for bandit at
    # the moment.
    # PLEASE REMOVE THE nosec AS SOON AS THE CERTIFICATE ISSUE IS FIXED!
    response = requests.get(SAMPLE_HASH_URL, timeout=30, verify=False)  # nosec
    _check_response(response)

    os.makedirs(BASE_DIR, exist_ok=True)
    _write_bytes_to_file(response.content, sample_hash_zip)
    _extract_zip(sample_hash_zip, BASE_DIR)
    _convert_hashset_sample_files(BASE_DIR)


def _extract_macos_table_data(markdown_lines: list) -> pd.DataFrame:
    """Extract the markdown table from the downloaded file.

    :param markdown_lines: Lines containing the content of the Markdown.
    :type markdown_lines: list

    :return: Extracted table.
    :rtype: pd.DataFrame
    """
    data = [
        line.split("|")[1:]
        for line in markdown_lines
        if line.startswith("|") and "-----------------" not in line
    ]
    data = [[item.strip() for item in line] for line in data]

    return pd.DataFrame(data[1:], columns=data[0])


def _process_macos_hashsums(hashsums: list) -> dict:
    """Clean hashtable to the haslookup format.

    :param hashsums: Lines of the markdown table.
    :type hashsums: list

    :return: Cleaned entries available in hashlookup format.
    :rtype: dict
    """
    comment_pattern = re.compile(r"<!--.*-->")
    hashsums = [item.replace("`", "").strip() for item in hashsums]
    hashsums_update = []
    file_names = []
    for item in hashsums:
        res = comment_pattern.search(item)
        if res is None:
            new_item = item
        else:
            new_item = item.replace(res.group(), "").strip()

        for subitem in new_item.split(","):
            splits = subitem.strip().split(" ")
            shasum = splits[0].upper()
            file_name = (
                splits[1].replace("(", "").replace(")", "")
                if len(splits) > 1
                else ""
            )
            hashsums_update.append(shasum)
            file_names.append(file_name)

    return pd.DataFrame(
        list(zip(file_names, hashsums_update)), columns=["FileName", "SHA-1"]
    ).to_dict(orient="records")


def _remove_empty_dict_entries(data: dict) -> list:
    """Remove any keys that have empty values within a dict.

    :param data: Dictionary to be cleaned.
    :type data: dict

    :return: Same dictionary, but without keys that were empty.
    :rtype: list
    """
    return [
        {key: value for key, value in item.items() if value}
        for item in data  # pylint: disable=not-an-iterable
    ]


def get_macos_hashset() -> None:
    """Download and extract the macOSX hashset into JSON format."""
    response = requests.get(
        "https://raw.githubusercontent.com/notpeter/apple-installer-checksums/master/readme.md",  # pylint: disable=line-too-long
        timeout=15,
    )
    _check_response(response)
    BASE_DIR = "dataset/apple"

    os.makedirs(BASE_DIR, exist_ok=True)
    df = _extract_macos_table_data(response.text.split("\n"))
    json_data = _process_macos_hashsums(df[df.columns[1]].to_list())
    json_data = _remove_empty_dict_entries(json_data)
    _save_json(json_data, os.path.join(BASE_DIR, "macosx.json"))


def _extract_xcode_tables(markdown_lines: list) -> dict:
    """Extract tables from the raw markdown file.

    :param markdown_lines: Strings containing the lines of the markdown
        file.
    :type markdown_lines: list

    :return: Extracted tables and sources.
    :rtype: dict
    """
    tables = {
        "installers": [],
        "installers_sources": {},
        "command_line_tools": [],
        "command_line_tools_sources": {},
    }
    header = ""
    for line in markdown_lines:
        if line.startswith("##"):
            if "installers" in line.casefold():
                header = "installers"
            elif "command line tools" in line.casefold():
                header = "command_line_tools"
        if line.startswith("|") and "-------" not in line:
            items = line.split("|")[1:]
            items = [item.strip() for item in items if len(item) > 0]
            tables[header].append(items)
        elif line.strip().startswith("["):
            version, url = line.strip().split("]:")
            tables[f"{header}_sources"][
                version.replace("[", "").strip()
            ] = url.strip()

    return tables


def _clean_xcode_tables(tables: dict) -> dict:
    """Clean the tables and add their sources.

    :param tables: Raw tables which are unformatted.
    :type tables: dict
    :return: Cleaned tables, ready to be converted into hashlookup
        format.
    :rtype: dict
    """
    new_tables = {"installers": [], "command_line_tools": []}
    version_pattern = re.compile(r"\[(([\d]{1,2})|(cli))\.?.*\]")
    comment_pattern = re.compile(r"<!--.*-->")
    line_mapping = {
        "installers": {"hash": 1, "version": 0, "filename": 2},
        "command_line_tools": {"hash": 0, "version": 1, "filename": 2},
    }
    for key, new_table in new_tables.items():
        hash_entry = line_mapping[key]["hash"]
        version_entry = line_mapping[key]["version"]
        filename_entry = line_mapping[key]["filename"]
        for line in tables[key][1:]:
            new_line = [
                line[version_entry],
                line[hash_entry],
                line[filename_entry],
            ]
            res = version_pattern.search(new_line[0])
            if res is not None:
                version = res.group().replace("[", "").replace("]", "")
                new_line.append(tables[f"{key}_sources"].get(version))
            new_line[1] = new_line[1].replace("`", "")
            res = comment_pattern.search(new_line[2])
            if res is not None:
                new_line[2] = new_line[2].replace(res.group(), "").strip()
            new_table.append(new_line)

    return new_tables


def _merge_xcode_tables(tables: dict) -> dict:
    """Merge XCode tables for installers and command line tools.

    :param tables: Contains tables indicated by keys "installers" and
    "command_line_tools".
    :type tables: dict

    :return: Concatenated tables in hashlookup format.
    :rtype: dict
    """
    headers = ["Version", "SHA-1", "FileName", "source"]
    dfs = {key: pd.DataFrame(tables[key], columns=headers) for key in tables}

    return pd.concat(list(dfs.values()))[headers[1:]].to_dict(orient="records")


def get_xcode_hashset() -> None:
    """Download and extract the XCode hashset into JSON format."""
    response = requests.get(
        "https://raw.githubusercontent.com/notpeter/apple-installer-checksums/master/xcode.md",  # pylint: disable=line-too-long
        timeout=15,
    )
    _check_response(response)
    BASE_DIR = "dataset/apple"

    os.makedirs(BASE_DIR, exist_ok=True)
    tables = _extract_xcode_tables(response.text.split("\n"))
    tables = _clean_xcode_tables(tables)
    json_data = _merge_xcode_tables(tables)
    json_data = _remove_empty_dict_entries(json_data)
    _save_json(json_data, os.path.join(BASE_DIR, "xcode.json"))


if __name__ == "__main__":
    get_kaspersky_hashset()
    get_hashset_sample()
    get_macos_hashset()
    get_xcode_hashset()
